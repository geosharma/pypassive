.. pypassive documentation master file, created by
   sphinx-quickstart on Sat Dec 30 09:40:59 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to pypassive documentation!
===================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   intro
   modules


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
