Introduction
============

``pypassive`` is a rudimentary geotechnical tool for computing passive pressure using the log spiral method using Mokwa (1999) and Duncan and Mokwa (2001), and Alqarawi et al. (2021). In addition it also includes the elasticity solution of Douglas and Davis (1964), and hyperbolic relationship of Duncan and Mokwa (2001). 

WARNING: Some tests are failing from code changes. And this document rendering is not working correctly, the module links are not working.

Motivation
**********

The motivation was to determine the force displacement curve for passive earth pressure.

Limitations
***********

- Only computes passive earth pressure.
- Mokwa (1999) and Duncan and Mokwa (2001) does not consider sloping backfill.
- Alqarawi et al. (2021) does not consider the resistance from cohesion.

Installation/Usage:
*******************
There are three alternatives:
 1. There is a version published in PyPi. `pip install pypassive`