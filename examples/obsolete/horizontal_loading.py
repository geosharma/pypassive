import numpy as np
from typing import Any
from scipy.optimize import minimize_scalar


def calc_hd(w, alpha):
    # Eq. F.3.c
    return w * np.tan(alpha)


def calc_yo(xo, alpha):
    # Eq. F.3.d
    return xo * np.tan(alpha)


def calc_ro(wall_height, xo, yo):
    # Eq F.3.e
    return np.sqrt((wall_height + yo) ** 2 + xo**2)


def calc_theta_max(wall_height, xo, yo, alpha):
    # Eq. F.3.f
    return np.pi / 2 - np.arctan(xo / (wall_height + yo)) - alpha


def calc_r_log_spiral(ro, theta, phi):
    return ro * np.exp(theta * np.tan((phi)))


def calc_r_diagonal(w, hd, xo, yo):
    return np.sqrt(w**2 + hd**2) + np.sign(xo) * np.sqrt(xo**2 + yo**2)


def calc_r(xo, w, wall_height, phi):
    phi = np.radians(phi)
    alpha = np.pi / 4 - phi / 2
    # print(f"alpha: {np.degrees(alpha):0.3f}")
    # step 1: Compute Hd using Eq F.3.c
    hd = calc_hd(w, alpha)
    # print(f"hd: {hd:0.3f}")
    # step 3: compute yo using Eqn F.3.d
    yo = calc_yo(xo, alpha)
    # print(f"yo: {yo:0.3f}")
    # step 4: compute ro using Eqn
    ro = calc_ro(wall_height, xo, yo)
    # print(f"ro: {ro:0.3f}")
    # step 5: compute theta, Eqn. F.3.f
    theta = calc_theta_max(wall_height, xo, yo, alpha)
    # print(f"theta: {theta:0.3f}")
    # step 6: compute using log spiral Eq F.2
    r_log_spiral = calc_r_log_spiral(ro, theta, phi)
    # print(f"r_log_spiral: {r_log_spiral:0.3f}")
    # step 7: compute r using Eq F.3.g
    r_diagonal = calc_r_diagonal(w, hd, xo, yo)
    # print(f"r_diagonal: {r_diagonal:0.3f}")
    return np.abs(r_log_spiral - r_diagonal)


def calc_hd2(r, yo, alpha):
    return r * np.sin(alpha) - yo


def calc_l1(wall_height, yo):
    return 2 / 3 * wall_height + yo


def calc_k(wall_height, w, hd):
    """Eqn F.4.c"""
    return w * (wall_height + 2 * hd) / (3 * (wall_height + hd))


def calc_l2(w, xo, wall_height, hd):
    """Eqn F.4.d"""
    k = w * (wall_height + 2 * hd) / (3 * (wall_height + hd))
    return xo + k


def calc_l3(yo, hd):
    """Eqn F.4.e"""
    return 2 / 3 * hd + yo


def calc_l4(w, xo):
    """Eq F.8"""
    return xo + w / 2


def calc_l5(yo, hd):
    """Eq F.9"""
    return yo + hd / 2


def calc_logsprial_soil_weight(w, xo, phi, wall_height, gamma, hd, ro, r1):
    """Eqn F.5"""
    return gamma * (
        (r1**2 - ro**2) / (4 * np.tan(phi))
        - 1 / 2 * xo * wall_height
        + 1 / 2 * w * hd
    )


def calc_Eprphi(phi, gamma, hd):
    """Rankine earth pressure due to soil weight, Eqn F.6"""
    return 1 / 2 * gamma * hd**2 * np.tan(np.pi / 4 + phi / 2) ** 2


def calc_Ppphi(w, xo, phi, delta, gamma, wall_height):
    """Earth pressure due to self weightEqn F.7"""
    phi = np.radians(phi)
    # print(f"phi: {np.degrees(phi)}")
    alpha1 = np.pi / 4 - phi / 2
    # print(f"alpha: {np.degrees(alpha1)}")
    delta = np.radians(delta)
    # print(f"delta: {np.degrees(delta)}")
    yo = calc_yo(xo, alpha1)
    # print(f"yo: {yo}")
    ro = calc_ro(wall_height, xo, yo)
    # print(f"ro: {ro}")
    theta = calc_theta_max(wall_height, xo, yo, alpha1)
    # print(f"theta: {theta}")
    r = calc_r_log_spiral(ro, theta, phi)
    # print(f"r: {r}")
    hd = calc_hd2(r, yo, alpha1)
    # print(f"hd: {hd}")
    log_spiral_weight = calc_logsprial_soil_weight(
        w, xo, phi, wall_height, gamma, hd, ro, r
    )
    # print(f"W: {log_spiral_weight}")
    Eprphi = calc_Eprphi(phi, gamma, hd)
    # print(f"Eprphi: {Eprphi}")
    l1 = calc_l1(wall_height, yo)
    # print(f"l1: {l1}")
    l2 = calc_l2(w, xo, wall_height, hd)
    # print(f"l2: {l2}")
    l3 = calc_l3(yo, hd)
    # print(f"l3: {l3}")
    # print(f"sin(delta): {np.sin(delta)}")
    # print(f"cos(delta): {np.cos(delta)}")
    return (l2 * log_spiral_weight + l3 * Eprphi) / (
        l1 * np.cos(delta) - xo * np.sin(delta)
    )


def calc_Eprc(hd, phi, c):
    """Rankine earth pressure due to cohesion acting on the vertical face Eqn F.10"""
    return 2 * c * np.tan(np.pi / 4 + phi / 2) * hd


def calc_Mc(r1, ro, phi, c):
    """Moment due to cohesion about point O Eqn. F.11"""
    return c / (2 * np.tan(phi)) * (r1**2 - ro**2)


def calc_Ppc(xo, c, phi, delta, wall_height, alpha):
    """Earth pressure due to cohesion, Eq F.12"""
    phi = np.radians(phi)
    # print(f"phi: {np.degrees(phi)}")
    alpha1 = np.pi / 4 - phi / 2
    # print(f"alpha: {np.degrees(alpha1)}")
    delta = np.radians(delta)
    # print(f"delta: {np.degrees(delta)}")
    yo = calc_yo(xo, alpha1)
    # print(f"yo: {yo}")
    ro = calc_ro(wall_height, xo, yo)
    # print(f"ro: {ro}")
    theta = calc_theta_max(wall_height, xo, yo, alpha1)
    # print(f"theta: {theta}")
    r = calc_r_log_spiral(ro, theta, phi)
    # print(f"r: {r}")
    hd = calc_hd2(r, yo, alpha1)
    # print(f"hd: {hd}")
    mc = calc_Mc(r, ro, phi, c)
    # print(f"mc: {mc}")
    l1 = calc_l1(wall_height, yo)
    # print(f"l1: {l1}")
    l5 = calc_l5(yo, hd)
    # print(f"l5: {l5}")
    Eprc = calc_Eprc(hd, phi, c)
    # print(f"Eprc: {Eprc}")
    # return (mc + l5 * Eprc + alpha * c * wall_height * xo) / (
    #     l1 * 0.998 - xo * 0.061
    # )
    return (mc + l5 * Eprc + alpha * c * wall_height * xo) / (
        l1 * np.cos(delta) - xo * np.sin(delta)
    )


def calc_Eprq(q, phi, hd):
    """Rankine earth pressure due to surcharge F.13"""
    return q * np.tan(np.pi / 4 + phi / 2) ** 2 * hd


def calc_Ppq(w, xo, c, phi, delta, wall_height, q):
    """Eqn F.14"""
    phi = np.radians(phi)
    # print(f"phi: {np.degrees(phi)}")
    alpha = np.pi / 4 - phi / 2
    # print(f"alpha: {np.degrees(alpha)}")
    delta = np.radians(delta)
    # print(f"delta: {np.degrees(delta)}")
    yo = calc_yo(xo, alpha)
    # print(f"yo: {yo}")
    ro = calc_ro(wall_height, xo, yo)
    # print(f"ro: {ro}")
    theta = calc_theta_max(wall_height, xo, yo, alpha)
    # print(f"theta: {theta}")
    r = calc_r_log_spiral(ro, theta, phi)
    # print(f"r: {r}")
    hd = calc_hd2(r, yo, alpha)
    # print(f"hd: {hd}")
    Eprq = calc_Eprq(q, phi, hd)
    # print(f"Eprq: {Eprq}")
    l1 = calc_l1(wall_height, yo)
    # print(f"l1: {l1}")
    l4 = calc_l4(w, xo)
    # print(f"l4: {l4}")
    l5 = calc_l5(yo, hd)
    # print(f"l5: {l5}")
    return (l4 * w * q + l5 * Eprq) / (l1 * np.cos(delta) - xo * np.sin(delta))


def calc_passive_earth_pressure(w, c, phi, delta, wall_height, gamma, q, alphac):
    xo = 5 * wall_height
    # res = minimize_scalar(calc_r, xo, args=(w, wall_height, phi), tol=1e-6)
    res = minimize_scalar(calc_r, args=(w, wall_height, phi), bounds=(-5 * wall_height, 5 * wall_height), method="bounded" )
    # print(res)
    xo = res.x
    # print(xo)
    Ppphi = calc_Ppphi(w, xo, phi, delta, gamma, wall_height)
    # print(f"Ppphi: {Ppphi}")
    Ppc = calc_Ppc(xo, c, phi, delta, wall_height, alphac)
    # print(f"Ppc: {Ppc}")
    Ppq = calc_Ppq(w, xo, c, phi, delta, wall_height, q)
    # print(f"Ppq: {Ppq}")
    return Ppphi + Ppc + Ppq


def main():
    # Wall dimension
    wall_height = 3.5
    # soil parameters
    phi = 37
    delta = 4
    gamma = 122
    c = 970
    alphac = 0.0
    q = 0.0

    # ref Mokwa 1999, Appendix F, pg 359
    # start with assuming a value for w
    # w = 4.098
    # step 2: Assume an inital value of xo
    # xo = 10.32
    # bnds = ((0, 5*wall_height),)
    res = minimize_scalar(
        calc_passive_earth_pressure,
        args=(c, phi, delta, wall_height, gamma, q, alphac),
        bounds = (0.5 * wall_height, 4 * wall_height),
        method="bounded",
    )
    print(res)
    # results = []
    # for w in np.linspace(wall_height, 2*wall_height):
    #     results.append(calc_passive_earth_pressure(w, c, phi, delta, wall_height, gamma, q, alphac)[0])
    # calc_passive_earth_pressure(w, c, phi, delta, wall_height, gamma, q, alphac)
    # res = minimize(calc_r, xo, args=(w, wall_height, phi), tol=1e-6)
    # print(res)
    # xo = 10.32

    # Ppphi = calc_Ppphi(w, xo, phi, delta, gamma, wall_height)
    # print(f"Ppphi: {Ppphi}")
    # Ppc = calc_Ppc(xo, c, phi, delta, wall_height, alpha)
    # print(f"Ppc: {Ppc}")
    # Ppq = calc_Ppq(w, xo, c, phi, delta, wall_height, q)
    # print(f"Ppq: {Ppq}")
    # for r in results:
    #     print(min(results))

if __name__ == "__main__":
    main()
