import numpy as np
from scipy.optimize import minimize


class LogSpiral:
    """Compute passive pressure on retaining wall.
    ref: Mokwa, R. L. (1999). Investigation of Resistance of Pile Caps to Lateral Loading.
        Duncan, M. J., and Mokwa, R. L. (2001). Passive Earth Pressures: Theories and Tests
    """

    def __init__(self, soil_layer, foundation):
        """Initialize the log spiral method for passive pressure with soil_layer and foundation.

        :param :class: SoilLayer soil_layer:
        :param :class: Foundation foundation:
        """
        
        self._c = soil_layer.c
        self._phi = np.radians(soil_layer.phi)
        self._gamma = soil_layer.gamma
        self._modE = soil_layer.modE
        self._nu = soil_layer.nu
        self._q = soil_layer.q
        self._h = foundation.h
        self._b = foundation.b
        self._z = foundation.z
        self._s = foundation.s
        self._alpha = np.pi/4 - self._phi/2

    @property
    def c(self):
        return self._c
    
    @property
    def phi(self):
        return self._phi
    
    @property
    def gamma(self):
        return self._gamma
    
    @property
    def modE(self):
        return self._modE

    @property
    def nu(self):
        return self._nu
    
    @property
    def q(self):
        return self._q
    
    @property
    def h(self):
        return self._h
    
    @property
    def b(self):
        return self._b
    
    @property
    def z(self):
        return self._z
    
    @property
    def s(self):
        return self._s
    
    @property
    def alpha(self):
        return self._alpha



    def calc_hd(self, w: float, alpha: float) -> float:
        """Calculate the height of Rankine earth presssure region behind the log spiral region.
        Eq. F.3.c

        :param float w: length of the failure region on the surface
        :param float alpha: angle of the Rankine passive failure surface

        :returns float: the height of the Rankine earth pressure region
        """
        return w * np.tan(alpha)

    def calc_yo(self, xo: float, alpha: float) -> float:
        """Compute ordinate of the log spiral center.
        Eq. F.3.d

        :param float xo: abscissa of the log spiral ordinate.
        :param float alpha: angle of the Rankine passive failure surface.

        :returns float yo: ordinate of the log spiral center.
        """
        return xo * np.tan(alpha)

    def calc_ro(self, wall_height: float, xo: float, yo: float) -> float:
        """Compute, ro, the starting radius of the log spiral.
        Eq F.3.e

        :param float wall_height: wall_height
        :param float xo: abscissa of the log spiral center
        :param float yo: ordinate of the log spiral center

        :returns float ro: starting radius of the log spiral, ro
        """
        return np.sqrt((wall_height + yo) ** 2 + xo**2)

    def calc_theta_max(
        self, wall_height: float, xo: float, yo: float, alpha: float
    ) -> float:
        """Compute, log spiral angle theta.
        Eq F.3.f

        :param float wall_height: wall_height
        :param float xo: abscissa of the log spiral center
        :param float yo: ordinate of the log spiral center
        :param float alpha: angle of the Rankine passive failure surface

        :returns float ro: starting radius of the log spiral, ro
        """

        return np.pi / 2 - np.arctan(xo / (wall_height + yo)) - alpha

    def calc_r_log_spiral(self, ro: float, theta: float, phi: float) -> float:
        """Compute, log spiral angle theta.
        Eq F.2

        :param float ro: starting radius of the log spiral
        :param float theta: log spiral angle, in radians
        :param float phi: Mohr-Coulomb phi, friction, parameter, in radians

        :returns float r: ending radius of the log spiral
        """
        return ro * np.exp(theta * np.tan((phi)))

    def calc_r_diagonal(self, w: float, hd: float, xo: float, yo: float) -> float:
        """Compute, maximum radius of the log spiral.
        Eq F.3.g

        :param float w: length of the failure surface on the ground surface
        :param float hd: the height of the Rankine earth pressure region
        :param float xo: abscissa of the log spiral center
        :param float yo: ordinate of the log spiral center

        :returns float r: ending radius of the log spiral
        """
        return np.sqrt(w**2 + hd**2) + np.sign(xo) * np.sqrt(xo**2 + yo**2)

    def calc_r(self, xo: float, w: float, wall_height: float, phi: float) -> float:
        """Minimization of the log spiral radius
        
        :param float w: length of the failure surface on the ground surface
        :param float wall_height: wall_height
        :param float phi: Mohr-Coulomb phi, friction, parameter, in radians

        
        """
        phi = np.radians(phi)
        alpha = np.pi / 4 - phi / 2
        # print(f"alpha: {np.degrees(alpha):0.3f}")
        # step 1: Compute Hd using Eq F.3.c
        hd = self.calc_hd(w, alpha)
        # print(f"hd: {hd:0.3f}")
        # step 3: compute yo using Eqn F.3.d
        yo = self.calc_yo(xo, alpha)
        # print(f"yo: {yo:0.3f}")
        # step 4: compute ro using Eqn
        ro = self.calc_ro(wall_height, xo, yo)
        # print(f"ro: {ro:0.3f}")
        # step 5: compute theta, Eqn. F.3.f
        theta = self.calc_theta_max(wall_height, xo, yo, alpha)
        # print(f"theta: {theta:0.3f}")
        # step 6: compute using log spiral Eq F.2
        r_log_spiral = self.calc_r_log_spiral(ro, theta, phi)
        # print(f"r_log_spiral: {r_log_spiral:0.3f}")
        # step 7: compute r using Eq F.3.g
        r_diagonal = self.calc_r_diagonal(w, hd, xo, yo)
        # print(f"r_diagonal: {r_diagonal:0.3f}")
        return np.abs(r_log_spiral - r_diagonal)

    def calc_hd2(self, r: float, yo: float, alpha: float) -> float:
        """Height of the Rankine failure region.
        F.4.a

        :param float r: end radius of the log spiral
        :param float yo: ordinate of the center of the log spiral
        :param float alpha: failure angle of the Rankine passive failure surface

        :returns float hd2: height of the Rankine failure surface
        """
        return r * np.sin(alpha) - yo

    def calc_l1(self, wall_height: float, yo: float) -> float:
        """Moment arm l1
        F.4.b

        :param float wall_height: height of the wall
        :param float yo: ordinate of the center of the log spiral

        :returns float l1: moment arm l1
        """
        return 2 / 3 * wall_height + yo

    def calc_k(self, wall_height: float, w: float, hd: float) -> float:
        """Eqn F.4.c

        :param float wall_height: height of the wall
        :param float w: the length of the failure surface on the ground
        :param float hd: height of the Rankine failure surface

        :returns float k:
        """
        return w * (wall_height + 2 * hd) / (3 * (wall_height + hd))

    def calc_l2(self, w: float, xo: float, wall_height: float, hd: float) -> float:
        """Moment arm l2
        Eqn F.4.d

        :param float w: the length of the failure surface on the ground
        :param float xo: abscissa of the log spiral center
        :param float wall_height: height of the wall
        :param float hd: height of the Rankine failure surface

        :returns float l2: moment arm l2
        """
        return xo + w * (wall_height + 2 * hd) / (3 * (wall_height + hd))

    def calc_l3(self, yo: float, hd: float) -> float:
        """Moment arm l3
        Eqn F.4.e

        :param float yo: ordinate of the log spiral
        :param float hd: height of the Rankine passive failure surface

        :return float l3:
        """
        return 2 / 3 * hd + yo

    def calc_l4(self, w: float, xo: float) -> float:
        """Moment arm l4
        Eq F.8

        :param float w: the length of the failure surface on the ground
        :param float xo: abscissa of the log spiral center

        :returns float l4:
        """
        return xo + w / 2

    def calc_l5(self, yo: float, hd: float) -> float:
        """Moment arm l5
        Eq F.9

        :param float yo: ordinate of the log spiral
        :param float hd: failure surface of the

        :returns float l5:
        """
        return yo + hd / 2

    def calc_logsprial_soil_weight(
        self,
        w: float,
        xo: float,
        phi: float,
        wall_height: float,
        gamma: float,
        hd: float,
        ro: float,
        r1: float,
    ) -> float:
        """Compute the weight of the log spiral region
        Eqn F.5

        :param float w: lenght of the failure surface on the ground
        :param float xo: abscissa of the log spiral center
        :param float phi: Mohr-Coulomb, phi, friction parameter
        :param float wall_height: height of the wall
        :param float gamma: unit weight of the soil
        :param float hd: height of the Rankine passive failure surface
        :param float ro: starting radius of the log spiral
        :param float r1: ending radius of the log spiral

        :returns float: the weight of the log spiral
        """
        return gamma * (
            (r1**2 - ro**2) / (4 * np.tan(phi))
            - 1 / 2 * xo * wall_height
            + 1 / 2 * w * hd
        )

    def calc_Eprphi(self, phi: float, gamma: float, hd: float) -> float:
        """Rankine earth pressure due to soil weight,
        Eqn F.6

        :param float phi: Mohr-Coulomb, phi, friction parameter
        :param float gamma: unit weight of the soil
        :param float hd: height of the Rankine passive failure surface

        :returns float: Rankine earth pressure due to soil weight
        """
        return 1 / 2 * gamma * hd**2 * np.tan(np.pi / 4 + phi / 2) ** 2

    def calc_Ppphi(
        self,
        w: float,
        xo: float,
        phi: float,
        delta: float,
        gamma: float,
        wall_height: float,
    ) -> float:
        """Earth pressure due to self weightEqn F.7"""
        phi = np.radians(phi)
        # print(f"phi: {np.degrees(phi)}")
        alpha = np.pi / 4 - phi / 2
        # print(f"alpha: {np.degrees(alpha)}")
        delta = np.radians(delta)
        # print(f"delta: {np.degrees(delta)}")
        yo = self.calc_yo(xo, alpha)
        # print(f"yo: {yo}")
        ro = self.calc_ro(wall_height, xo, yo)
        # print(f"ro: {ro}")
        theta = self.calc_theta_max(wall_height, xo, yo, alpha)
        # print(f"theta: {theta}")
        r = self.calc_r_log_spiral(ro, theta, phi)
        # print(f"r: {r}")
        hd = self.calc_hd2(r, yo, alpha)
        # print(f"hd: {hd}")
        log_spiral_weight = self.calc_logsprial_soil_weight(
            w, xo, phi, wall_height, gamma, hd, ro, r
        )
        # print(f"W: {log_spiral_weight}")
        Eprphi = self.calc_Eprphi(phi, gamma, hd)
        # print(f"Eprphi: {Eprphi}")
        l1 = self.calc_l1(wall_height, yo)
        # print(f"l1: {l1}")
        l2 = self.calc_l2(w, xo, wall_height, hd)
        # print(f"l2: {l2}")
        l3 = self.calc_l3(yo, hd)
        # print(f"l3: {l3}")
        # print(f"sin(delta): {np.sin(delta)}")
        # print(f"cos(delta): {np.cos(delta)}")
        return (l2 * log_spiral_weight + l3 * Eprphi) / (
            l1 * np.cos(delta) - xo * np.sin(delta)
        )

    def calc_Eprc(self, hd: float, phi: float, c: float) -> float:
        """Rankine earth pressure due to cohesion acting on the vertical face Eqn F.10"""
        return 2 * c * np.tan(np.pi / 4 + phi / 2) * hd

    def calc_Mc(self, r1: float, ro: float, phi: float, c: float) -> float:
        """Moment due to cohesion about point O Eqn. F.11"""
        return c / (2 * np.tan(phi)) * (r1**2 - ro**2)

    def calc_Ppc(
        self,
        xo: float,
        c: float,
        phi: float,
        delta: float,
        wall_height: float,
        alphac: float,
    ) -> float:
        """Earth pressure due to cohesion, Eq F.12"""
        phi = np.radians(phi)
        # print(f"phi: {np.degrees(phi)}")
        alpha = np.pi / 4 - phi / 2
        # print(f"alpha: {np.degrees(alpha)}")
        delta = np.radians(delta)
        # print(f"delta: {np.degrees(delta)}")
        yo = self.calc_yo(xo, alpha)
        # print(f"yo: {yo}")
        ro = self.calc_ro(wall_height, xo, yo)
        # print(f"ro: {ro}")
        theta = self.calc_theta_max(wall_height, xo, yo, alpha)
        # print(f"theta: {theta}")
        r = self.calc_r_log_spiral(ro, theta, phi)
        # print(f"r: {r}")
        hd = self.calc_hd2(r, yo, alpha)
        # print(f"hd: {hd}")
        mc = self.calc_Mc(r, ro, phi, c)
        # print(f"mc: {mc}")
        l1 = self.calc_l1(wall_height, yo)
        # print(f"l1: {l1}")
        l5 = self.calc_l5(yo, hd)
        # print(f"l5: {l5}")
        Eprc = self.calc_Eprc(hd, phi, c)
        # print(f"Eprc: {Eprc}")
        # return (mc + l5 * Eprc + alpha * c * wall_height * xo) / (
        #     l1 * 0.998 - xo * 0.061
        # )
        return (mc + l5 * Eprc + alphac * c * wall_height * xo) / (
            l1 * np.cos(delta) - xo * np.sin(delta)
        )

    def calc_Eprq(self, q : float, phi : float, hd : float) -> float:
        """Rankine earth pressure due to surcharge F.13"""
        return q * np.tan(np.pi / 4 + phi / 2) ** 2 * hd

    def calc_Ppq(self, w: float, xo : float, c : float, phi : float, delta : float, wall_height : float, q : float):
        """Eqn F.14"""
        phi = np.radians(phi)
        # print(f"phi: {np.degrees(phi)}")
        alpha = np.pi / 4 - phi / 2
        # print(f"alpha: {np.degrees(alpha)}")
        delta = np.radians(delta)
        # print(f"delta: {np.degrees(delta)}")
        yo = self.calc_yo(xo, alpha)
        # print(f"yo: {yo}")
        ro = self.calc_ro(wall_height, xo, yo)
        # print(f"ro: {ro}")
        theta = self.calc_theta_max(wall_height, xo, yo, alpha)
        # print(f"theta: {theta}")
        r = self.calc_r_log_spiral(ro, theta, phi)
        # print(f"r: {r}")
        hd = self.calc_hd2(r, yo, alpha)
        # print(f"hd: {hd}")
        Eprq = self.calc_Eprq(q, phi, hd)
        # print(f"Eprq: {Eprq}")
        l1 = self.calc_l1(wall_height, yo)
        # print(f"l1: {l1}")
        l4 = self.calc_l4(w, xo)
        # print(f"l4: {l4}")
        l5 = self.calc_l5(yo, hd)
        # print(f"l5: {l5}")
        return (l4 * w * q + l5 * Eprq) / (l1 * np.cos(delta) - xo * np.sin(delta))

    def calc_passive_earth_pressure(self, w):
        xo = 3 * self.h
        print(xo, w, self.wall_height, np.degrees(self.phi))
        res = minimize(self.calc_r, xo, args=(w, self.wall_height, self.phi), tol=1e-6)
        xo = res.x
        # print(res)
        Ppphi = self.calc_Ppphi(w, xo, self.phi, self.delta, self.gamma, self.wall_height)
        # print(f"Ppphi: {Ppphi}")
        Ppc = self.calc_Ppc(xo, self.c, self.phi, self.delta, self.wall_height, self.alphac)
        # print(f"Ppc: {Ppc}")
        Ppq = self.calc_Ppq(w, xo, self.c, self.phi, self.delta, self.wall_height, self.q)
        # print(f"Ppq: {Ppq}")
        return Ppphi + Ppc + Ppq