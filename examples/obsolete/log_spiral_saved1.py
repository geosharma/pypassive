import math
from scipy.optimize import minimize, minimize_scalar


class LogSpiral:
    """Compute passive pressure on retaining wall.
    ref: Mokwa, R. L. (1999). Investigation of Resistance of Pile Caps to Lateral Loading.
        Duncan, M. J., and Mokwa, R. L. (2001). Passive Earth Pressures: Theories and Tests
    """

    def __init__(self, soil_layer, foundation):
        """Initialize the log spiral method for passive pressure with soil_layer and foundation.

        :param :class: SoilLayer soil_layer:
        :param :class: Foundation foundation:
        """

        self._c: float = soil_layer.c
        self._phi: float = math.radians(soil_layer.phi)
        self._gamma: float = soil_layer.gamma
        self._delta: float = math.radians(soil_layer.delta)
        self._modE: float = soil_layer.modE
        self._nu: float = soil_layer.nu
        self._alphac: float = soil_layer.alphac
        self._q: float = soil_layer.q
        self._h: float = foundation.h
        self._b: float = foundation.b
        self._z: float = foundation.z
        self._s: float = foundation.s
        self._alpha: float = math.pi / 4 - self._phi / 2
        self._w: float = 0.0
        self._xo: float = 3 * foundation.h
        self._yo: float = 0.0
        self._hd: float = None
        self._r_logspiral: float = 0.0
        self._r: float = 0.0
        self._ro: float = 0.0
        self._theta: float = 0.0
        self._l1: float = 0.0
        self._l2: float = 0.0
        self._l3: float = 0.0
        self._l4: float = 0.0
        self._l5: float = 0.0
        self._logspiral_weight: float = 0.0
        self._Eprphi: float = 0.0
        self._Eprc: float = 0.0
        self._Ppphi: float = 0.0
        self._Ppc: float = 0.0
        self._mc: float = 0.0
        self._Eprq: float = 0.0
        self._Ppphi : float = 0.0
        self._Ppc: float = 0.0
        self._Ppq : float = 0.0

    @property
    def c(self):
        return self._c

    @property
    def phi(self):
        return self._phi

    @property
    def gamma(self):
        return self._gamma

    @property
    def modE(self):
        return self._modE

    @property
    def nu(self):
        return self._nu

    @property
    def q(self):
        return self._q

    @property
    def h(self):
        return self._h

    @property
    def b(self):
        return self._b

    @property
    def z(self):
        return self._z

    @property
    def s(self):
        return self._s

    @property
    def alpha(self):
        return self._alpha

    @property
    def ro(self):
        return self._ro

    @property
    def yo(self):
        return self._yo

    @property
    def xo(self):
        return self._xo

    @xo.setter
    def xo(self, val):
        self._xo = val

    @property
    def theta(self):
        return self._theta

    @property
    def w(self):
        self._w

    @w.setter
    def w(self, val):
        self._w = val

    @property
    def hd(self):
        return self._hd

    def calc_hd(self, w: float) -> float:
        """Calculate the height of Rankine earth presssure region behind the log spiral region.
        Eq. F.3.c

        :param float w: length of the failure region on the surface
        :param float alpha: angle of the Rankine passive failure surface

        :returns float: the height of the Rankine earth pressure region
        """
        if self._hd is None:
            return w * math.tan(self._alpha)
        else:
            return self._r * math.sin(self._alpha) - self._yo

    def calc_yo(self, xo: float) -> float:
        """Compute ordinate of the log spiral center.
        Eq. F.3.d

        :param float xo: abscissa of the log spiral ordinate.float = 5 * self._h
        :param float alpha: angle of the Rankine passive failure surface.

        :returns float yo: ordinate of the log spiral center.
        """
        return xo * math.tan(self._alpha)

    def calc_ro(self, xo: float) -> float:
        """Compute, ro, the starting radius of the log spiral.
        Eq F.3.e

        :param float wall_height: wall_height
        :param float xo: abscissa of the log spiral center
        :param float yo: ordinate of the log spiral center

        :returns float ro: starting radius of the log spiral, ro
        """

        return math.sqrt((self._h + self._yo) ** 2 + xo**2)

    def calc_theta_max(self, xo: float) -> float:
        """Compute, log spiral angle theta.
        Eq F.3.f

        :param float wall_height: wall_height
        :param float xo: abscissa of the log spiral center
        :param float yo: ordinate of the log spiral center
        :param float alpha: angle of the Rankine passive failure surface

        :returns float ro: starting radius of the log spiral, ro
        """

        return math.pi / 2 - math.atan(xo / (self._h + self._yo)) - self._alpha

    def calc_r_logspiral(self) -> float:
        """Compute, log spiral angle theta.
        Eq F.2

        :param float ro: starting radius of the log spiral
        :param float theta: log spiral angle, in radians
        :param float phi: Mohr-Coulomb phi, friction, parameter, in radians

        :returns float r: ending radius of the log spiral
        """

        return self._ro * math.exp(self._theta * math.tan((self._phi)))

    def calc_r_diagonal(self, w: float, xo: float) -> float:
        """Compute, maximum radius of the log spiral.
        Eq F.3.g

        :param float w: length of the failure surface on the ground surface
        :param float hd: the height of the Rankine earth pressure region
        :param float xo: abscissa of the log spiral center
        :param float yo: ordinate of the log spiral center

        :returns float r: ending radius of the log spiral
        """

        return math.sqrt(w**2 + self._hd**2) + math.copysign(1, xo) * math.sqrt(
            xo**2 + self._yo**2
        )

    def calc_r(self, xo: float, w: float) -> float:
        """Minimization of the log spiral radius

        :param float xo: abscissa of the origin of the log spiral
        :param float w: length of the failure surface on the ground surface
        """

        self._hd = None
        self._xo = xo

        # print("xo:", xo)
        # print(f"alpha: {math.degrees(self.alpha):0.3f}")
        # step 1: Compute Hd using Eq F.3.c
        self._hd = self.calc_hd(w)
        # print(f"hd: {self.hd:0.3f}")
        # step 3: compute yo using Eqn F.3.d
        self._yo = self.calc_yo(xo)
        # print(f"yo: {self.yo:0.3f}")
        # step 4: compute ro using Eqn
        self._ro = self.calc_ro(xo)
        # print(f"ro: {self.ro:0.3f}")
        # step 5: compute theta, Eqn. F.3.f
        self._theta = self.calc_theta_max(xo)
        # print(f"theta: {self.theta:0.3f}")
        # step 6: compute using log spiral Eq F.2
        self._r_logspiral = self.calc_r_logspiral()
        # print(f"r_logspiral: {self._r_logspiral}")
        # step 7: compute r using Eq F.3.g
        self._r = self.calc_r_diagonal(w, xo)
        # print(f"r_diagonal: {self._r}")
        return abs(self._r_logspiral - self._r)

    # def calc_hd2(self, r: float, yo: float, alpha: float) -> float:
    #     """Height of the Rankine failure region.
    #     F.4.a

    #     :param float r: end radius of the log spiral
    #     :param float yo: ordinate of the center of the log spiral
    #     :param float alpha: failure angle of the Rankine passive failure surface

    #     :returns float hd2: height of the Rankine failure surface
    #     """
    #     return r * math.sin(alpha) - yo

    def calc_l1(self) -> float:
        """Moment arm l1
        F.4.b

        :param float wall_height: height of the wall
        :param float yo: ordinate of the center of the log spiral

        :returns float l1: moment arm l1
        """
        return 2 / 3 * self._h + self._yo

    def calc_k(self, w: float) -> float:
        """Eqn F.4.c

        :param float wall_height: height of the wall
        :param float w: the length of the failure surface on the ground
        :param float hd: height of the Rankine failure surface

        :returns float k:
        """
        return w * (self._h + 2 * self._hd) / (3 * (self._h + self._hd))

    def calc_l2(self, w: float) -> float:
        """Moment arm l2
        Eqn F.4.d

        :param float w: the length of the failure surface on the ground
        :param float xo: abscissa of the log spiral center
        :param float wall_height: height of the wall
        :param float hd: height of the Rankine failure surface

        :returns float l2: moment arm l2
        """
        return self._xo + w * (self._h + 2 * self._hd) / (3 * (self._h + self._hd))

    def calc_l3(self) -> float:
        """Moment arm l3
        Eqn F.4.e

        :param float yo: ordinate of the log spiral
        :param float hd: height of the Rankine passive failure surface

        :return float l3:
        """
        return 2 / 3 * self._hd + self._yo

    def calc_l4(self, w: float) -> float:
        """Moment arm l4
        Eq F.8

        :param float w: the length of the failure surface on the ground
        :param float xo: abscissa of the log spiral center

        :returns float l4:
        """
        return self._xo + w / 2

    def calc_l5(self) -> float:
        """Moment arm l5
        Eq F.9

        :param float yo: ordinate of the log spiral
        :param float hd: failure surface of the

        :returns float l5:
        """
        return self._yo + self._hd / 2

    def calc_logsprial_soil_weight(self, w: float) -> float:
        """Compute the weight of the log spiral region
        Eqn F.5

        :param float w: lenght of the failure surface on the ground
        :param float xo: abscissa of the log spiral center
        :param float phi: Mohr-Coulomb, phi, friction parameter
        :param float wall_height: height of the wall
        :param float gamma: unit weight of the soil
        :param float hd: height of the Rankine passive failure surface
        :param float ro: starting radius of the log spiral
        :param float r1: ending radius of the log spiral

        :returns float: the weight of the log spiral
        """
        return self._gamma * (
            (self._r**2 - self._ro**2) / (4 * math.tan(self._phi))
            - 1 / 2 * self._xo * self._h
            + 1 / 2 * w * self._hd
        )

    def calc_Eprphi(self) -> float:
        """Rankine earth pressure due to soil weight,
        Eqn F.6

        :param float phi: Mohr-Coulomb, phi, friction parameter
        :param float gamma: unit weight of the soil
        :param float hd: height of the Rankine passive failure surface

        :returns float: Rankine earth pressure due to soil weight
        """
        return (
            1
            / 2
            * self._gamma
            * self._hd**2
            * math.tan(math.pi / 4 + self._phi / 2) ** 2
        )

    def calc_Ppphi(self, w: float) -> float:
        """Earth pressure due to self weight of the soil

        :param float w:
        Eqn F.7"""

        self._hd = self.calc_hd(w)
        # print(f"hd: {hd}")
        self._logspiral_weight = self.calc_logsprial_soil_weight(w)
        # print(f"W: {log_spiral_weight}")
        self._Eprphi = self.calc_Eprphi()
        # print(f"Eprphi: {Eprphi}")
        self._l1 = self.calc_l1()
        # print(f"l1: {l1}")
        self._l2 = self.calc_l2(w)
        # print(f"l2: {l2}")
        self._l3 = self.calc_l3()
        # print(f"l3: {l3}")
        # print(f"sin(delta): {math.sin(delta)}")
        # print(f"cos(delta): {math.cos(delta)}")
        return (self._l2 * self._logspiral_weight + self._l3 * self._Eprphi) / (
            self._l1 * math.cos(self._delta) - self._xo * math.sin(self._delta)
        )

    def calc_Eprc(self) -> float:
        """Rankine earth pressure due to cohesion acting on the vertical face Eqn F.10"""
        return 2 * self._c * math.tan(math.pi / 4 + self._phi / 2) * self._hd

    def calc_Mc(self) -> float:
        """Moment due to cohesion about point O Eqn. F.11"""
        return self._c / (2 * math.tan(self._phi)) * (self._r**2 - self._ro**2)

    def calc_Ppc(self) -> float:
        """Earth pressure due to cohesion, Eq F.12"""

        # print(f"hd: {hd}")
        self._mc = self.calc_Mc()
        # print(f"mc: {mc}")
        # print(f"l1: {l1}")
        self._l5 = self.calc_l5()
        # print(f"l5: {l5}")
        self._Eprc = self.calc_Eprc()
        # print(f"Eprc: {Eprc}")
        # return (mc + l5 * Eprc + alpha * c * wall_height * xo) / (
        #     l1 * 0.998 - xo * 0.061
        # )
        return (
            self._mc
            + self._l5 * self._Eprc
            + self._alphac * self._c * self._h * self._xo
        ) / (self._l1 * math.cos(self._delta) - self._xo * math.sin(self._delta))

    def calc_Eprq(self) -> float:
        """Rankine earth pressure due to surcharge F.13"""
        return self._q * math.tan(math.pi / 4 + self._phi / 2) ** 2 * self._hd

    def calc_Ppq(self, w: float):
        """Eqn F.14"""

        # print(f"hd: {hd}")
        self._Eprq = self.calc_Eprq()
        # print(f"Eprq: {Eprq}")
        self._l4 = self.calc_l4(w)
        # print(f"l4: {l4}")
        return (self._l4 * w * self._q + self._l5 * self._Eprq) / (
            self._l1 * math.cos(self._delta) - self._xo * math.sin(self._delta)
        )

    def calc_passive_earth_pressure(self, w: float)-> float:
        xo = 3 * self.h
        # w = 4.06
        print(xo, w, self.h, math.degrees(self.phi))
        res = minimize_scalar(
            self.calc_r, args=(w), bounds=(-5 * self.h, 5 * self.h), method="bounded"
        )
        self._xo = res.x

        Ppphi = self.calc_Ppphi(w)
        # print(f"Ppphi: {Ppphi}")
        Ppc = self.calc_Ppc()
        # print(f"Ppc: {Ppc}")
        Ppq = self.calc_Ppq(w)
        # print(f"Ppq: {Ppq}")
        return Ppphi + Ppc + Ppq
