# example_006
# Description: Compute passive force using log spiral method
# ref: Alqarawi, A. S., Leo, C. J., Liyanapathirana, D. S., Sigdel, L., Lu, M., and Hu, P. (2012).
#    A spreadsheet-based technique to calculate the passive soil pressure based on log-spiral method.
#    Computers and Geotechnics 130.


from pypassive import (
    SoilLayer,
    RetainingWall,
    AlqarawiLogSpiral,
)


def main():
    # soil parameters
    # Mohr-Coulomb c parameter [psf], cohesion
    c = 0
    # Mohr-Coulomb phi parameter [deg], friction angle
    phi = 35.5
    # unit weight of the soil, [pcf]
    gamma = 15.3
    # soil-structure interface angle [deg]
    delta = 24
    # foundation parameters
    # foundation/wall height [ft]
    h = 0.20

    soillayer = SoilLayer(c, phi, gamma, delta)
    foundation = RetainingWall(h)

    lgs = AlqarawiLogSpiral(soillayer, foundation)

    # res = lgs.calc_passive_force_all(-0.20)
    # print(f"Mabg: {lgs._Mabg:0.4f}")
    res = lgs.passive_force()
    print(f"Pp: {res}")
    # print(f"status: {res.status}")
    # print(
    #     f"w: {lgs.w:0.3f}, xo: {lgs.xo:0.3f}, Pp: {lgs.Pp:0.3}, Ppphi: {lgs.Ppphi:0.3f}, Ppq: {lgs.Ppq:0.3f}, Ppc: {lgs.Ppc}"
    # )
    # print(f"r: {lgs.r:0.3f}")
    # print(
    #     f"alpha: {np.degrees(lgs.alpha):0.3f}, Hd: {lgs.hd:0.3f}, xo: {lgs.xo:0.3f}, yo: {lgs.yo:0.3f}, ro: {lgs.ro:0.3f}"
    # )
    # print(
    #     f"theta: {lgs.theta:0.3f}, r_logspiral: {lgs._r_logspiral:0.5f}, r:{lgs.r:0.5f}"
    # )
    # print(
    #     f"l1: {lgs.l1:0.3f}, l2: {lgs.l2:0.3f}, l3: {lgs.l3:0.3f}, l4: {lgs.l4:0.3f}, l5: {lgs.l5:0.3f}"
    # )
    # print(
    #     f"tanphi: {np.tan(lgs.phi):0.3f}, cosdelta: {np.cos(lgs.delta):0.3f}, sindelta: {np.sin(lgs.delta):0.3f}"
    # )
    # print(f"Weight log spiral: {lgs.log_spiral_weight:0.3f}")


if __name__ == "__main__":
    main()
