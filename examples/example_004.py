# example_03.py
# Description: Compute hyperbolic load deflection curve. Use Douglas and Davis 1964 elasticity
# solution for horizontally loaded vertical plate to compute kmax.
# ref: Mokwa, R. L. (1999). Investigation of Resistance of Pile Caps to Lateral Loading.
# pg. 269, 270, 379--382
import numpy as np
from pypassive import (
    SoilLayer,
    Foundation,
    DouglasDavis1964,
    MokwaDuncan1999_hyperbolic,
)


def main():
    # soil parameters
    # Mohr-Coulomb c parameter [psf], cohesion
    c = 970
    # Mohr-Coulomb phi parameter [deg], friction angle
    phi = 37
    # unit weight of the soil, [pcf]
    gamma = 122

    # Young's modulus of the soil layer [psf]
    modE = 890000
    # Poisson's ratio of the soil layer [-]
    nu = 0.33
    # adhesion ratio between the soil and the wall [-] range 0 -- 1 adhesion = alphac * c

    # foundation parameters
    # foundation/wall height [ft]
    h = 3.5
    # foundation width [ft]
    b = 6.3
    # Ultimate load [lb]
    pult = 160400

    # soil parameters
    soillayer = SoilLayer(
        c,
        phi,
        gamma,
        modE=modE,
        nu=nu,
    )
    # foundation parameters
    foundation = Foundation(h, b)

    # Douglas and Davis (1964) elastic solution for horizontal loading on a vertical rectangle
    dd = DouglasDavis1964(soillayer, foundation)
    print(f"kmax: {dd.kmax/(12 * 1000):0.3f} [kips/in]")
    delta_max = 0.04 * foundation.h
    md = MokwaDuncan1999_hyperbolic(pult, dd.kmax, delta_max)
    ys = np.linspace(0.05, 1.85, 37)
    ys = np.concatenate((np.array([0, 0.01, 0.03]), ys), axis=None) / 12
    ys, ps = md.hyperbolic_force_displacement(ys)
    print("{0:>6s}, {1:>10s}".format("y [in]", "P [kip]"))
    for y, p in zip(ys, ps):
        print(f"{y * 12:6.2f}, {p/1000:>10.2f}")


if __name__ == "__main__":
    main()
