# example_001
# Description: Test calculation of log spiral r, by minimizing the difference between the diagonal
# distance to the end of the log spiral and the log spiral radius to the same point.
# Given w, the width of the extent of the log spiral, determine the center of the log spiral from
# the radius of the log spiral. Compare the coordinates of the center of the log spiral, the log
# spiral angle, theta, the log spiral radius, r, and the height of the Rankine zone with those
# given in the Mokwa (1999)
# This example is test_01
# ref: Mokwa, R. L. (1999). Investigation of Resistance of Pile Caps to Lateral Loading.
# Appendix F, pg. 359 -- 372


from scipy.optimize import minimize_scalar
from pypassive import (
    SoilLayer,
    RetainingWall,
    DuncanMokwaLogSpiral,
)


def main():
    # soil parameters
    # Mohr-Coulomb c parameter [psf], cohesion
    c = 970
    # Mohr-Coulomb phi parameter [deg], friction angle
    phi = 37
    # unit weight of the soil, [pcf]
    gamma = 122
    # soil-structure interface angle [deg]
    delta = 3.5
    # Young's modulus of the soil layer [psf]
    modE = 890000
    # Poisson's ratio of the soil layer [-]
    nu = 0.33
    # adhesion ratio between the soil and the wall [-] range 0 -- 1 adhesion = alphac * c
    alphac = 0
    # surchage on top of the wall [psf]
    q = 0
    # foundation parameters
    # foundation/wall height [ft]
    h = 3.5
    # foundation width [ft]
    b = 6.3

    sl = SoilLayer(c, phi, gamma, delta, modE, nu, alphac, q)
    rw = RetainingWall(h, b)

    lgs = DuncanMokwaLogSpiral(sl, rw)

    # width of the log spiral extent on the surface
    w = 4.0604
    bnds = (-5.0 * h, 5 * h)

    res = minimize_scalar(
        lgs.calc_r,
        args=(w),
        bounds=bnds,
        method="bounded",
    )

    print(f"status: {res.status}")
    print(f"w: {w:0.3f}, xo: {lgs.xo:0.4f}, yo: {lgs.yo:0.3f}, Hd: {lgs.hd:0.3f}")

    print(
        f"theta: {lgs.theta:0.3f}, r_logspiral: {lgs.r:0.6f}, r:{lgs._dist_r:0.6f}, diff: {abs(lgs._dist_r - lgs.r):0.6f}"
    )


if __name__ == "__main__":
    main()
