# example_005
# Description: Test calculation of log spiral r, by minimizing the difference between the diagonal
# distance to the end of the log spiral and the log spiral radius to the same point.
# ref: Mokwa, R. L. (1999). Investigation of Resistance of Pile Caps to Lateral Loading.
# Appendix F, pg. 359 -- 372
# Refer: Duncan, M. J., and Mokwa, R. L. (2001). Passive earth pressures: Theories and tests.
# for Ovesen-Brinch Hansen method of Correcting for 3D Effects in Passive Earth Pressures.

from pypassive import (
    SoilLayer,
    RetainingWall,
    DuncanMokwaLogSpiral,
)


def main():
    # soil parameters
    # Mohr-Coulomb c parameter [psf], cohesion
    c = 970
    # Mohr-Coulomb phi parameter [deg], friction angle
    phi = 37
    # unit weight of the soil, [pcf]
    gamma = 122
    # soil-structure interface angle [deg]
    delta = 3.5
    # Young's modulus of the soil layer [psf]
    modE = 890000
    # Poisson's ratio of the soil layer [-]
    nu = 0.33
    # adhesion ratio between the soil and the wall [-] range 0 -- 1 adhesion = alphac * c
    alphac = 0
    # surchage on top of the wall [psf]
    q = 0
    # foundation parameters
    # foundation/wall height [ft]
    h = 3.5
    # foundation width [ft]
    b = 6.3

    sl = SoilLayer(c, phi, gamma, delta, modE, nu, alphac, q)
    rw = RetainingWall(h, b)

    lgs = DuncanMokwaLogSpiral(sl, rw)

    # width of the log spiral extent on the surface
    w = 4.0604
    xo = 10.3195
    r = 16.068106

    diff_r = lgs.calc_r(xo, w)
    lgs.calc_moment_arms(w)

    print(
        f"l1: {lgs.l1:0.2f}, lgs.l2: {lgs.l2:0.2f}, l3: {lgs.l3:0.2f}, lgs.l4: {lgs.l4:0.2f}, lgs.l5: {lgs.l5:0.2f}"
    )
    # print(f"log spiral weight: {lgs.log_spiral_weight:0.1f}")


if __name__ == "__main__":
    main()
