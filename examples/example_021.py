import numpy as np
from pypassive import SoilLayer, Foundation, DouglasDavis1964, MokwaDuncan1999_hyperbolic


def main():
    # soil parameters
    # Mohr-Coulomb c parameter [psf], cohesion
    c = 970
    # Mohr-Coulomb phi parameter [deg], friction angle
    phi = 37
    # unit weight of the soil, [pcf]
    gamma = 122
    
    # Young's modulus of the soil layer [psf]
    modE = 890000
    # Poisson's ratio of the soil layer [-]
    nu = 0.33
    # adhesion ratio between the soil and the wall [-] range 0 -- 1 adhesion = alphac * c
    
    # foundation parameters
    # foundation/wall height [ft]
    h = 3.5
    # foundation width [ft]
    b = 6.3
    
    pult = 160.4
    soillayer = SoilLayer(c, phi, gamma, modE=modE, nu=nu,)
    foundation = Foundation(h, b)
    dd = DouglasDavis1964(soillayer, foundation)
    print(f"kmax: {dd.kmax:0.3f}")
    delta_max = 0.04 * foundation.h
    md = MokwaDuncan1999_hyperbolic(pult, dd.kmax, delta_max)
    ys = np.linspace(0.05, 1.85, 37)
    ys = np.concatenate((np.array([0, 0.01, 0.03]), ys), axis=None)/12
    ys, ps = md.hyperbolic_force_displacement(ys)
    for y, p in zip(ys, ps):
        print(f"{y * 12:0.2f} {p:0.2f}")

if __name__ == "__main__":
    main()
