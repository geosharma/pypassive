# pypassive
Determination of passive earth pressure on retaining walls using log spiral method. This is a work in progress. The values may be a few percent different from those given in the references. 

## Reference
1.  Mokwa, R. L. (1999). Investigation of Resistance of Pile Caps to Lateral Loading.

2. Duncan, M. J., and Mokwa, R. L. (2001). Passive Earth Pressures: Theories and Tests. journal of Geotechnical and Geoenvironmental Engineering, Volume 127, Issue 3.

3. Alqarawi, A. S., Leo, C. J., Liyanapathirana, D. S., Sigdel, L., Lu, M., and Hu, P. (2021). A spreadsheet-based technique to calculate the passive soil pressure based on log-spiral method. Computers and Geotechnics 130

## Things to do
[x] Passive pressure using Mokwa and Duncan Method.

[x] Passive pressure using Alqarawi et al. (2021) method.

[x] Duncan and Mokwa hyperbolic method to determine passive force displacement curve.

[ ] Documentation. The documentation is not complete

[ ] Tests. There are some tests in the test folder

[ ] Examples - There are few in the examples folder but is not yet complete.